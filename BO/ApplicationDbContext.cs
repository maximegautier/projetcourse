﻿using BO.Model;
using BO.Model.Jeux;
using BO.Model.Personnes.Equipes;
using BO.Model.Tournois;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
            System.Diagnostics.Debug.WriteLine("Nouvelle Instance du context");
        }

        public virtual DbSet<TournoiJeuVideo> TournoiJeuVideos { get; set; }

        public virtual DbSet<Equipe> Equipes { get; set; }

        public virtual DbSet<JeuVideo> JeuVideos { get; set; }

        public virtual DbSet<Match> Matchs { get; set; }

        //public virtual DbSet<Personne> Personnes { get; set; }

        public virtual DbSet<Support> Supports { get; set; }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}
