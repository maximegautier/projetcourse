namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitApplicationUserJoueur : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Equipes",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Nom = c.String(),
                        Point = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Matches",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Tournoi_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.Tournois", t => t.Tournoi_id)
                .Index(t => t.Tournoi_id);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Nom = c.String(),
                        Prenom = c.String(),
                        DateNaissance = c.DateTime(nullable: false),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                        Equipe_id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Equipes", t => t.Equipe_id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Equipe_id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Tournois",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        Libelle = c.String(),
                        Date = c.DateTime(nullable: false),
                        CashPrize = c.Single(nullable: false),
                        Reglement = c.String(),
                        Prix = c.Single(nullable: false),
                        Mode = c.Int(),
                        NombreJoueurMax = c.Int(),
                        Discriminator = c.String(nullable: false, maxLength: 128),
                        JeuVideo_id = c.Int(),
                    })
                .PrimaryKey(t => t.id)
                .ForeignKey("dbo.JeuVideos", t => t.JeuVideo_id)
                .Index(t => t.JeuVideo_id);
            
            CreateTable(
                "dbo.JeuVideos",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        AgeLimite = c.Int(nullable: false),
                        Nom = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.Supports",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        libelle = c.String(),
                    })
                .PrimaryKey(t => t.id);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.MatchEquipes",
                c => new
                    {
                        Match_id = c.Int(nullable: false),
                        Equipe_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Match_id, t.Equipe_id })
                .ForeignKey("dbo.Matches", t => t.Match_id, cascadeDelete: true)
                .ForeignKey("dbo.Equipes", t => t.Equipe_id, cascadeDelete: true)
                .Index(t => t.Match_id)
                .Index(t => t.Equipe_id);
            
            CreateTable(
                "dbo.TournoiEquipes",
                c => new
                    {
                        Tournoi_id = c.Int(nullable: false),
                        Equipe_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Tournoi_id, t.Equipe_id })
                .ForeignKey("dbo.Tournois", t => t.Tournoi_id, cascadeDelete: true)
                .ForeignKey("dbo.Equipes", t => t.Equipe_id, cascadeDelete: true)
                .Index(t => t.Tournoi_id)
                .Index(t => t.Equipe_id);
            
            CreateTable(
                "dbo.SupportJeuVideos",
                c => new
                    {
                        Support_id = c.Int(nullable: false),
                        JeuVideo_id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Support_id, t.JeuVideo_id })
                .ForeignKey("dbo.Supports", t => t.Support_id, cascadeDelete: true)
                .ForeignKey("dbo.JeuVideos", t => t.JeuVideo_id, cascadeDelete: true)
                .Index(t => t.Support_id)
                .Index(t => t.JeuVideo_id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.Tournois", "JeuVideo_id", "dbo.JeuVideos");
            DropForeignKey("dbo.SupportJeuVideos", "JeuVideo_id", "dbo.JeuVideos");
            DropForeignKey("dbo.SupportJeuVideos", "Support_id", "dbo.Supports");
            DropForeignKey("dbo.Matches", "Tournoi_id", "dbo.Tournois");
            DropForeignKey("dbo.TournoiEquipes", "Equipe_id", "dbo.Equipes");
            DropForeignKey("dbo.TournoiEquipes", "Tournoi_id", "dbo.Tournois");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "Equipe_id", "dbo.Equipes");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.MatchEquipes", "Equipe_id", "dbo.Equipes");
            DropForeignKey("dbo.MatchEquipes", "Match_id", "dbo.Matches");
            DropIndex("dbo.SupportJeuVideos", new[] { "JeuVideo_id" });
            DropIndex("dbo.SupportJeuVideos", new[] { "Support_id" });
            DropIndex("dbo.TournoiEquipes", new[] { "Equipe_id" });
            DropIndex("dbo.TournoiEquipes", new[] { "Tournoi_id" });
            DropIndex("dbo.MatchEquipes", new[] { "Equipe_id" });
            DropIndex("dbo.MatchEquipes", new[] { "Match_id" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.Tournois", new[] { "JeuVideo_id" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "Equipe_id" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.Matches", new[] { "Tournoi_id" });
            DropTable("dbo.SupportJeuVideos");
            DropTable("dbo.TournoiEquipes");
            DropTable("dbo.MatchEquipes");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.Supports");
            DropTable("dbo.JeuVideos");
            DropTable("dbo.Tournois");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Matches");
            DropTable("dbo.Equipes");
        }
    }
}
