namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EquipeGagnante : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Matches", "NomEquipeGagnante", c => c.String());
            AddColumn("dbo.Tournois", "EquipeGagnante", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Tournois", "EquipeGagnante");
            DropColumn("dbo.Matches", "NomEquipeGagnante");
        }
    }
}
