namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCoachId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Equipes", "CoachId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Equipes", "CoachId");
        }
    }
}
