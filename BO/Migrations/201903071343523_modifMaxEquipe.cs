namespace BO.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class modifMaxEquipe : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tournois", "NombreEquipeMax", c => c.Int());
            DropColumn("dbo.Tournois", "NombreJoueurMax");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Tournois", "NombreJoueurMax", c => c.Int());
            DropColumn("dbo.Tournois", "NombreEquipeMax");
        }
    }
}
