namespace BO.Migrations
{
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Model;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<BO.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(BO.ApplicationDbContext context)
        {
            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var storeRole = new RoleStore<IdentityRole>(context);
                var managerRole = new RoleManager<IdentityRole>(storeRole);

                var role = new IdentityRole { Name = "Admin" };
                managerRole.Create(role);

                role = new IdentityRole { Name = "Joueur" };
                managerRole.Create(role);

                role = new IdentityRole { Name = "Entraineur" };
                managerRole.Create(role);

                role = new IdentityRole { Name = "Organisateur" };
                managerRole.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "admin"))
            {
                var storeUser = new UserStore<ApplicationUser>(context);
                var managerUser = new UserManager<ApplicationUser>(storeUser);
                var user = new ApplicationUser { UserName = "admin", Email = "admin@admin.fr" };

                managerUser.Create(user, "administrateur");
                managerUser.AddToRole(user.Id, "Admin");
            }

        }
    }
}
