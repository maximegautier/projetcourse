﻿using System.Collections.Generic;

namespace BO.Model.Jeux
{
    public class JeuVideo : Jeu
    {
        public int AgeLimite { get; set; }
        public virtual List<Support> Supports { get; set; }
    }
}
