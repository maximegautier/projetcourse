﻿using BO.Personnes;
using System.Collections.Generic;

namespace BO.Model.Jeux
{
    public class Support : Entity
    {
        public string libelle { get; set; }

        public virtual List<JeuVideo> JeuxVideo { get; set; }
    }
}