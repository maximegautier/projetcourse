﻿using BO.Model.Tournois;
using BO.Personnes;
using System.Collections.Generic;


namespace BO.Model.Personnes.Equipes
{
    public class Equipe : Entity
    {
        public string Nom { get; set; }
        public int Point { get; set; }
        public string CoachId { get; set; }
        public virtual List<ApplicationUser> personnes { get; set; }
        public virtual List<Tournoi> Tournois { get; set; }
        public virtual List<Match> Matchs { get; set; }
    }
}
