﻿using BO.Model.Personnes.Equipes;
using BO.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Model.Tournois
{
    public class Match : Entity
    {
        public string NomEquipeGagnante { get; set; }
        public virtual List<Equipe> Equipes { get; set; }
    }
}
