﻿using System.ComponentModel.DataAnnotations;

namespace BO.Model.Tournois
{
    public enum Mode
    {
        Duo=2,
        Trio=3,
        Quatuor=4,
        Quartet=5,
        Plus=6,
    }
}