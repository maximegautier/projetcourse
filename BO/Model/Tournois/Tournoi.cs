﻿using BO.Model.Personnes.Equipes;
using BO.Personnes;
using System;
using System.Collections.Generic;

namespace BO.Model.Tournois
{
    public class Tournoi : Entity
    {
        public string Libelle { get; set; }
        public string EquipeGagnante { get; set; }
        public DateTime Date { get; set; }
        public float CashPrize { get; set; }
        public string Reglement { get; set; }
        public virtual List<Equipe> Equipes { get; set; }
        public float Prix { get; set; }
        public virtual List<Match> Matchs { get; set; }
    }
}
