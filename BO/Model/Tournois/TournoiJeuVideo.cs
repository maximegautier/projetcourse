﻿using BO.Model.Jeux;

namespace BO.Model.Tournois
{
    public class TournoiJeuVideo : Tournoi
    {
        public Mode Mode { get; set; }
        public int NombreEquipeMax { get; set; }
        public virtual JeuVideo JeuVideo { get; set; }
    }
}
