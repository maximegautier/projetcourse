﻿namespace BO
{
    using Model.Jeux;
    using Model.Personnes.Equipes;
    using Model.Tournois;
    using Personnes;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class ProjetTournoiContext : DbContext
    {
        // Votre contexte a été configuré pour utiliser une chaîne de connexion « ProjetTournoiModel » du fichier 
        // de configuration de votre application (App.config ou Web.config). Par défaut, cette chaîne de connexion cible 
        // la base de données « BO.ProjetTournoiModel » sur votre instance LocalDb. 
        // 
        // Pour cibler une autre base de données et/ou un autre fournisseur de base de données, modifiez 
        // la chaîne de connexion « ProjetTournoiModel » dans le fichier de configuration de l'application.
        public ProjetTournoiContext()
            : base("ProjetTournoiModel")
        {
        }

        // Ajoutez un DbSet pour chaque type d'entité à inclure dans votre modèle. Pour plus d'informations 
        // sur la configuration et l'utilisation du modèle Code First, consultez http://go.microsoft.com/fwlink/?LinkId=390109.

        public virtual DbSet<TournoiJeuVideo> TournoiJeuVideos { get; set; }

        public virtual DbSet<Equipe> Equipes { get; set; }

        public virtual DbSet<JeuVideo> JeuVideos { get; set; }

        public virtual DbSet<Match> Matchs { get; set; }

        public virtual DbSet<Personne> Personnes { get; set; }

        public virtual DbSet<Support> Supports { get; set; }

    }
}