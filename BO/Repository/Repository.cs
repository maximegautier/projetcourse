﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BO.Repository
{
    public class Repository<T> : IRepository<T> where T : class
    {
        private ApplicationDbContext _context;
        public Repository(ApplicationDbContext context)
        {
            this._context = context;
        }

        public virtual void Create(T item)
        {
            this._context.Set<T>().Add(item);
        }

        public virtual T Get(int? id)
        {
            return this._context.Set<T>().Find(id);
        }

        public virtual List<T> GetAll()
        {
            return this.GetAll(x => true);
        }

        public virtual List<T> GetAll(Func<T, bool> filter)
        {
            return this._context.Set<T>().Where(filter).ToList();
        }

        public virtual void Commit()
        {
            this._context.SaveChanges();
        }

        public virtual void Delete(int? id)
        {
            this._context.Set<T>().Remove(this.Get(id));
        }

        public void Dispose()
        {
            this._context.Dispose();
        }

        public virtual void Delete(string id)
        {
            this._context.Set<T>().Remove(this.Get(id));
        }

        public virtual T Get(string id)
        {
            return this._context.Set<T>().Find(id);
        }
    }
}
