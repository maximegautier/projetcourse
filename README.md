Membres du projet :

Geoffray BIGOT

Maxime GAUTIER

Eroan COLIN

Language utilisé : **ASP.Net MVC**

Framework : EntityFramework, Automapper

Besoin
------------------
Le but de l'application est de pouvoir gérer des tournois de jeux vidéo.

Joueurs/Coachs
------------------
Des joueurs/coachs peuvent créer ou rejoindre des équipes s'ils y sont invités.

Organisateurs 
------------------
    - Chaque Tournoi à une Date,
    - Un jeu doit y être spécifié (un jeu à un support(une console))
    - Le mode Duo/Trio/+ doit être intégré dans le tournoi
    - Un tournoi doit avoir un type (Bracket,Arbre,points,etc.)
    - Le nombre de joueurs/équipes max doit être défini
    - Le Cash Prize doit être fixé à la création du tournoi
    - Un règlement du tournoi peut être ajouter à la création.
    
Administrateurs tournoi
------------------
Un administrateur de tournoi sera désigné pour la gestion des victoires/défaites
des équipes.

Visiteurs ou Commentateurs.
------------------
Les tournois à venir ou en cours doivent être accéssible à tout le monde.
La visibilité des équipes est ouverte à tout le monde.