﻿using BO.Model.Personnes.Equipes;
using BO.Repository;
using System;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models.Personnes.Equipes;
using WebApp.Models.Equipes;
using BO.Model.Tournois;

namespace WebApp.Controllers
{
    public class AddEquipeAuTournoiController : Controller
    {

        private IRepository<Equipe> _repository;

        private IRepository<TournoiJeuVideo> _repositoryTournoi;
        private IRepository<Match> _repositoryMatch;

        public AddEquipeAuTournoiController(IRepository<Equipe> repository, IRepository<TournoiJeuVideo> repoTournoi, IRepository<Match> repoMatch)
        {
            this._repository = repository;
            this._repositoryTournoi = repoTournoi;
            this._repositoryMatch = repoMatch;
        }
        [Authorize(Roles = "Entraineur, Admin")]
        public ActionResult Index()
        {
            var Equipe = this._repository.GetAll(a => a.CoachId.Contains(User.Identity.GetUserId())).FirstOrDefault();
            var equipeVm = AutoMapper.Mapper.Map<AddEquipeToTournoiViewModel>(Equipe);
            equipeVm.InitData(); 
            return View(equipeVm);
        }

        [HttpPost]
        [Authorize(Roles = "Entraineur, Admin")]
        public ActionResult Index(AddEquipeToTournoiViewModel EquipeAdd)
        {
            var TournoisSelectionne = this._repositoryTournoi.GetAll(s => EquipeAdd.tournoiId.Contains(s.id.ToString()));
            var Equipe = this._repository.Get(EquipeAdd.Id);

            foreach (var e in TournoisSelectionne.Select(s => new { nbEquipes = s.NombreEquipeMax , taille = s.Equipes.Count() }))
            {
                if(e.nbEquipes < (e.taille + 1) )
                {
                    ModelState.AddModelError(string.Empty,"L'un des tournoi à déjà un nombre d'équipe suffisant !");
                }
            }
            if (ModelState.IsValid)
            {
                foreach(var e in TournoisSelectionne.Select(s => s.Equipes))
                {
                    e.Add(Equipe);
                }

                foreach (var e in TournoisSelectionne)
                {
                    e.Matchs = setMatchs(e);
                }

                this._repositoryTournoi.Commit();
                return RedirectToAction("../");
            }
            EquipeAdd.InitData();
            return View(EquipeAdd);
        }
        public List<Match> setMatchs(TournoiJeuVideo TournoisSelectionne)
        {
            var equipes = TournoisSelectionne.Equipes.OrderBy(e=>e.id);
            List<Match> lstmatch = TournoisSelectionne.Matchs;
            lstmatch = new List<Match>();
            foreach (var equipeA in equipes)
            {
                foreach (var equipeB in equipes.Where(e => e.id > equipeA.id))
                {
                    Match match = new Match();
                    match.Equipes = new List<Equipe>();
                    this._repositoryMatch.Create(match);
                    match.Equipes.Add(equipeA);
                    match.Equipes.Add(equipeB);
                    lstmatch.Add(match);
                }
            }
            lstmatch = ShuffleList(lstmatch);
            return lstmatch;
        }

        private List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count); //Choose a random object in the list
                randomList.Add(inputList[randomIndex]); //add it to the new, random list
                inputList.RemoveAt(randomIndex); //remove to avoid duplicates
            }

            return randomList;
        }
    }
}
