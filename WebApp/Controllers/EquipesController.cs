﻿using BO.Model;
using BO.Model.Personnes.Equipes;
using BO.Personnes;
using BO.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Extensions;
using WebApp.Mapper;
using WebApp.Models.Equipes;
using WebApp.Models.Personnes.Equipes;

namespace WebApp.Controllers
{
    public class EquipesController : Controller
    {
        private IRepository<Equipe> _repository;
        private IRepository<ApplicationUser> _repositoryP;

        public EquipesController()
        {

        }

        public EquipesController(IRepository<Equipe> repository, IRepository<ApplicationUser> repositoryP)
        {
            this._repository = repository;
            this._repositoryP = repositoryP;
        }
        // GET: Equipes
        public ActionResult Index()
        {
            var equipes = _repository.GetAll();
            return View(equipes.Select(s => s.Map<EquipeViewModel>()));
        }

        // GET: Equipes/Details/5
        public ActionResult Details(int id)
        {
            return View(this._repository.Get(id).Map<EquipeViewModel>());
        }

        // GET: Equipes/Create
        [Authorize(Roles = "Admin, Entraineur")]
        public ActionResult Create()
        {
            var CEEqupe = new CreateEditEquipeViewModel();
            CEEqupe.CoachId = User.Identity.GetUserId();
            CEEqupe.InitData();
            return View("CreateEdit", CEEqupe);
        }

        // GET: Equipes/Edit/5
        [Authorize(Roles = "Admin, Entraineur")]
        public ActionResult Edit(int id)
        {
            var CEEqupe = this._repository.Get(id).Map<CreateEditEquipeViewModel>();
            CEEqupe.InitData();
            return View("CreateEdit", CEEqupe);
        }

        // POST: Equipes/Edit/5
        [HttpPost]
        [Authorize(Roles = "Admin, Entraineur")]
        public ActionResult CreateEdit(int id, CreateEditEquipeViewModel equipeViewModel)
        {
            if (ModelState.IsValid)
            {
                var personnes = this._repositoryP.GetAll(s => equipeViewModel.joueursSelect.Contains(s.Id.ToString()));
                var equipe = this._repository.Get(equipeViewModel.Id);
                if (equipe == null)
                {
                    equipe = new Equipe();
                    this._repository.Create(equipe);
                }
                equipe = AutoMapper.Mapper.Map(equipeViewModel, equipe);
                equipe.personnes = personnes;

                this._repository.Commit();
                return RedirectToAction("Index");
            }
            return View(equipeViewModel);
        }

        // GET: Equipes/Delete/5
        [Authorize(Roles = "Admin, Entraineur")]
        public ActionResult Delete(int id)
        {
            return View(this._repository.Get(id).Map<EquipeViewModel>());
        }

        // POST: Equipes/Delete/5
        [HttpPost]
        [Authorize(Roles = "Admin, Entraineur")]
        public ActionResult Delete(int id, EquipeViewModel equipeViewModel)
        {
            try
            {
                this._repository.Delete(id);
                this._repository.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View(equipeViewModel);
            }
        }
    }
}
