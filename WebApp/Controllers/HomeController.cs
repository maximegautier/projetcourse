﻿using BO.Model.Tournois;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Extensions;
using WebApp.Models.Tournois;

namespace WebApp.Controllers
{
    public class HomeController : Controller
    {
        private IRepository<TournoiJeuVideo> _repositoryTournoiJeuVideo;

        public HomeController(IRepository<TournoiJeuVideo> _repositoryTournoiJeuVideo)
        {
            this._repositoryTournoiJeuVideo = _repositoryTournoiJeuVideo;
        }

        public ActionResult Index()
        {
            var tournoisjeuxvideo = this._repositoryTournoiJeuVideo.GetAll();
            return View(tournoisjeuxvideo.Select(j => j.Map<TournoiJeuVideoViewModel>()));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}