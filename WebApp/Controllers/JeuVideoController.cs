﻿using BO;
using BO.Model.Jeux;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using WebApp.Models;
using WebApp.Extensions;

namespace WebApp.Controllers
{
    public class JeuVideoController : Controller
    {
        private IRepository<JeuVideo> _repositoryJeuVideo;
        private IRepository<Support> _repositorySupport;

        public JeuVideoController(IRepository<JeuVideo> repositoryJeuVideo, IRepository<Support> repositorySupport)
        {
            this._repositoryJeuVideo = repositoryJeuVideo;
            this._repositorySupport = repositorySupport;
        }
        public ActionResult Index()
        {
            var jeuxvideo = this._repositoryJeuVideo.GetAll();
            return View(jeuxvideo.Select(j => j.Map<JeuVideoViewModel>()));
        }

        // GET: JeuVideo/Details/5
        public ActionResult Details(int id)
        {
            return View(this._repositoryJeuVideo.Get(id).Map<JeuVideoViewModel>());
        }

        // GET: JeuVideo/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View("CreateEdit", new CreateEditJeuVideo());
        }

        // GET: JeuVideo/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var jeuVideoCE = this._repositoryJeuVideo.Get(id).Map<CreateEditJeuVideo>();
            return View("CreateEdit", jeuVideoCE);
        }

        // POST: JeuVideo/CreateEdit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult CreateEdit(CreateEditJeuVideo createEditJeuVideo)
        {
            if (ModelState.IsValid)
            {
                List<Support> supports = this._repositorySupport.GetAll(s => createEditJeuVideo.SupportSelected.Contains(s.id.ToString()));
                JeuVideo jeuVideo = this._repositoryJeuVideo.Get(createEditJeuVideo.Id);
                if(jeuVideo == null)
                {
                    jeuVideo = new JeuVideo();
                    this._repositoryJeuVideo.Create(jeuVideo);
                }
                jeuVideo = AutoMapper.Mapper.Map(createEditJeuVideo, jeuVideo);
                jeuVideo.Supports = supports;

                this._repositoryJeuVideo.Commit();
                return RedirectToAction("Index");
            }

            return View(createEditJeuVideo);
        }

        // GET: JeuVideo/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            var jeuVideoVM = this._repositoryJeuVideo.Get(id).Map<JeuVideoViewModel>();
            return View(jeuVideoVM);
        }

        // POST: JeuVideo/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._repositoryJeuVideo.Delete(id);
                this._repositoryJeuVideo.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}