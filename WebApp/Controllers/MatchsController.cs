﻿using BO.Model.Personnes.Equipes;
using BO.Model.Tournois;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Extensions;
using WebApp.Models.Tournois;

namespace WebApp.Controllers
{
    public class MatchsController : Controller
    {
        private IRepository<Tournoi> _repositoryTournois;
        private IRepository<Match> _repositoryMatchs;
        private IRepository<Equipe> _repositoryEquipes;

        public MatchsController(IRepository<Tournoi> repositoryTournois, IRepository<Match> repositoryMatchs,
            IRepository<Equipe> repositoryEquipe)
        {
            this._repositoryTournois = repositoryTournois;
            this._repositoryMatchs = repositoryMatchs;
            this._repositoryEquipes = repositoryEquipe;
        }
        // GET: Matchs
        public ActionResult Index(int id)
        {
            var match = this._repositoryTournois.GetAll(m => m.id == id).Select(t => t.Matchs).FirstOrDefault();
            return View(match.Select(m => m.Map<MatchViewModel>()));
        }

        // GET: Matchs/Details/5
        [Authorize(Roles = "Admin, Organisateur")]
        public ActionResult Details(int id)
        {
            var match = this._repositoryMatchs.Get(id);
            return View(AutoMapper.Mapper.Map<MatchViewModel>(match));
        }

        // POST: Matchs/Details/5
        [HttpPost]
        [Authorize(Roles = "Admin, Organisateur")]
        public ActionResult Details(int id, FormCollection collection)
        {
            var match = this._repositoryMatchs.Get(id);
            if (ModelState.IsValid)
            {
                var index = collection.GetValue("unString");
                var EquipeGagnante = this._repositoryEquipes.Get(int.Parse(index.AttemptedValue));
                if(match.NomEquipeGagnante == null || match.NomEquipeGagnante.Equals(""))
                {
                    EquipeGagnante.Point += 3;
                } else
                {
                    match.Equipes.Where(c => c.Nom == match.NomEquipeGagnante).FirstOrDefault().Point -= 3;
                    EquipeGagnante.Point += 3;
                }
      
                match.NomEquipeGagnante = EquipeGagnante.Nom;
                this._repositoryMatchs.Commit();
                this._repositoryEquipes.Commit();


                return RedirectToAction("../Tournois/Index");
            }

            return View(AutoMapper.Mapper.Map<MatchViewModel>(match));

        }


    }
}
