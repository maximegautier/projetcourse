﻿using BO;
using BO.Model;
using BO.Model.Personnes.Equipes;
using BO.Personnes;
using BO.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Extensions;
using WebApp.Models;
using WebApp.Models.Personnes;

namespace WebApp.Controllers
{
    public class PersonnesController : Controller
    {
        private IRepository<Equipe> _repoEquipe;
        private IRepository<ApplicationUser> _repoUser;

        public PersonnesController()
        {
        }

        public PersonnesController(IRepository<Equipe> repoEquipe, IRepository<ApplicationUser> repoUser)
        {
            this._repoEquipe = repoEquipe;
            this._repoUser = repoUser;
            }


        // GET: Personnes
        public ActionResult Index()
        {
            List<PersonneViewModel> personnesVM = this._repoUser.GetAll().Select(a => a.Map<PersonneViewModel>()).ToList();
            return View(personnesVM);
        }

        // GET: Personnes/Details/5
        public ActionResult Details(string id)
        {
            PersonneViewModel personneVM = this._repoUser.GetAll().Where(a => a.Id == id).Select(a => a.Map<PersonneViewModel>()).FirstOrDefault();
            return View(personneVM);
        }

        // GET: Personnes/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(string id)
        {
            EditViewPersonne personneVM = this._repoUser.GetAll().Where(a => a.Id == id).Select(a => a.Map<EditViewPersonne>()).FirstOrDefault();
            ApplicationDbContext context = new ApplicationDbContext();

            var storeUser = new UserStore<ApplicationUser>(context);
            var managerUser = new UserManager<ApplicationUser>(storeUser);

            personneVM.RoleId = managerUser.GetRoles(personneVM.Id).FirstOrDefault();
            return View(personneVM);
        }

        // POST: Personnes/Edit/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Edit(string id, EditViewPersonne editViewPersonne, ApplicationDbContext context)
        {
            try
            {
                var storeUser = new UserStore<ApplicationUser>(context);
                var managerUser = new UserManager<ApplicationUser>(storeUser);
                ApplicationUser user = this._repoUser.GetAll(a => a.Id == id).First();
                  
                if (user == null)
                {
                    View(editViewPersonne);
                }

                user.Nom = editViewPersonne.Nom;
                user.Prenom = editViewPersonne.Prenom;
                user.UserName = editViewPersonne.Pseudonyme;
                user.DateNaissance = editViewPersonne.DateNaissance;
                user.Email = editViewPersonne.Mail;

                string userRole = managerUser.GetRoles(user.Id).FirstOrDefault();
                managerUser.RemoveFromRole(user.Id, userRole);
                managerUser.AddToRole(user.Id, editViewPersonne.RoleId);

                this._repoUser.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(editViewPersonne);
            }
        }

        // GET: Personnes/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(string id)
        {
            PersonneViewModel personneVM = this._repoUser.GetAll().Where(a => a.Id == id).Select(a => a.Map<EditViewPersonne>()).FirstOrDefault();
            return View(personneVM);
        }

        // POST: Personnes/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost]
        public ActionResult Delete(string id, PersonneViewModel personneViewModel)
        {
            try
            {
                this._repoUser.Delete(id);
                this._repoUser.Commit();

                return RedirectToAction("Index");
            }
            catch
            {
                return View(personneViewModel);
            }
        }
    }
}
