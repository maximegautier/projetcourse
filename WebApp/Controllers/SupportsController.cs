﻿using BO.Model.Jeux;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Extensions;
using WebApp.Models.Jeux;

namespace WebApp.Controllers
{
    public class SupportsController : Controller
    {
        private IRepository<Support> _repositorySupport;

        public SupportsController(IRepository<Support> repositorySupport)
        {
            this._repositorySupport = repositorySupport;
        }
        // GET: Supports
        public ActionResult Index()
        {
            var support = this._repositorySupport.GetAll();
            return View(support.Select(j => j.Map<SupportViewModel>()));
        }

        // GET: Supports/Details/5
        public ActionResult Details(int id)
        {
            return View(this._repositorySupport.Get(id).Map<SupportViewModel>());
        }

        // GET: Supports/Create
        [Authorize(Roles = "Admin")]
        public ActionResult Create()
        {
            return View("CreateEdit", new SupportViewModel());
        }

        // GET: Supports/Edit/5
        [Authorize(Roles = "Admin")]
        public ActionResult Edit(int id)
        {
            var supprotCE = this._repositorySupport.Get(id).Map<SupportViewModel>();
            return View("CreateEdit", supprotCE);
        }

        // POST: JeuVideo/CreateEdit/5
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult CreateEdit(SupportViewModel SupportVM)
        {
            if (ModelState.IsValid)
            {
                Support support = this._repositorySupport.Get(SupportVM.Id);
                if (support == null)
                {
                    support = new Support();
                    this._repositorySupport.Create(support);
                }
                support = AutoMapper.Mapper.Map(SupportVM, support);

                this._repositorySupport.Commit();
                return RedirectToAction("Index");
            }

            return View(SupportVM);
        }

        // GET: Supports/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id)
        {
            var supprotVM = this._repositorySupport.Get(id).Map<SupportViewModel>();
            return View(supprotVM);
        }

        // POST: Supports/Delete/5
        [HttpPost]
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._repositorySupport.Delete(id);
                this._repositorySupport.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
