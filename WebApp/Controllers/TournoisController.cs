﻿using BO.Model.Jeux;
using BO.Model.Personnes.Equipes;
using BO.Model.Tournois;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Extensions;
using WebApp.Models.Tournois;

namespace WebApp.Controllers
{
    public class TournoisController : Controller
    {
        private IRepository<TournoiJeuVideo> _repositoryTournoiJeuVideo;
        private IRepository<Match> _repositoryMatch;
        private IRepository<Equipe> _repositoryEquipe;
        private IRepository<JeuVideo> _repositoryJeuVideo;

        public TournoisController(IRepository<TournoiJeuVideo> _repositoryTournoiJeuVideo, 
                                    IRepository<Match> _repositoryMatch,
                                    IRepository<Equipe> _repositoryEquipe, 
                                    IRepository<JeuVideo> _repositoryJeuVideo)
        {
            this._repositoryTournoiJeuVideo = _repositoryTournoiJeuVideo;
            this._repositoryMatch = _repositoryMatch;
            this._repositoryEquipe = _repositoryEquipe;
            this._repositoryJeuVideo = _repositoryJeuVideo;
        }
        // GET: Tournois
        public ActionResult Index()
        {
            var tournoisjeuxvideo = this._repositoryTournoiJeuVideo.GetAll();
            return View(tournoisjeuxvideo.Select(j => j.Map<TournoiJeuVideoViewModel>()));
        }

        // GET: Tournois/Details/5
        public ActionResult Details(int id)
        {
            var tournoi = this._repositoryTournoiJeuVideo.Get(id);
             if(tournoi.Matchs.Count() != 0 && tournoi.Matchs.Where(c => c.NomEquipeGagnante == null || c.NomEquipeGagnante.Equals("")).Count() < 1)
            {
                tournoi.EquipeGagnante = tournoi.Equipes.Where(c => c.Point == tournoi.Equipes.Select(s => s.Point).Max()).Select(m => m.Nom).FirstOrDefault();
                foreach(var i in tournoi.Equipes)
                {
                    i.Point = 0;
                }
                this._repositoryEquipe.Commit();
                this._repositoryTournoiJeuVideo.Commit();
            }
       
            return View(tournoi.Map<TournoiJeuVideoViewModel>());
        }

        // GET: Tournois/Create
        [Authorize(Roles = "Admin, Organisateur")]
        public ActionResult Create()
        {
            var CETournoisVM = new CreateEditTournoisJeuVideoViewModel();
            CETournoisVM.InitList();
            return View("CreateEdit", CETournoisVM);
        }

        // GET: Tournois/Edit/5
        [Authorize(Roles = "Admin, Organisateur")]
        public ActionResult Edit(int id)
        {
            var tournoisjeuVideoCE = this._repositoryTournoiJeuVideo.Get(id).Map<CreateEditTournoisJeuVideoViewModel>();
            tournoisjeuVideoCE.InitList();
            return View("CreateEdit", tournoisjeuVideoCE);
        }

        // POST: JeuVideo/CreateEdit/5
        [Authorize(Roles = "Admin, Organisateur")]
        [HttpPost]
        public ActionResult CreateEdit(CreateEditTournoisJeuVideoViewModel ceTournoisJeuVideo)
        {
            if (ModelState.IsValid)
            {
                var JeuVideo = this._repositoryJeuVideo.Get(int.Parse(ceTournoisJeuVideo.JeuVideoId));
                var mode = Enum.GetValues(typeof(Mode)).Cast<Mode>().Where(s => ((int)s).ToString() == ceTournoisJeuVideo.ModeId).FirstOrDefault();
                TournoiJeuVideo tournoisjeuvideo = this._repositoryTournoiJeuVideo.Get(ceTournoisJeuVideo.Id);
                if (tournoisjeuvideo == null)
                {
                    tournoisjeuvideo = new TournoiJeuVideo();
                    this._repositoryTournoiJeuVideo.Create(tournoisjeuvideo);
                }
                tournoisjeuvideo = AutoMapper.Mapper.Map(ceTournoisJeuVideo, tournoisjeuvideo);
                tournoisjeuvideo.JeuVideo = JeuVideo;
                tournoisjeuvideo.Mode = mode;

                this._repositoryTournoiJeuVideo.Commit();
                return RedirectToAction("Index");
            }

            return View(ceTournoisJeuVideo);
        }

        // GET: Tournois/Delete/5
        [Authorize(Roles = "Admin, Organisateur")]
        public ActionResult Delete(int id)
        {
            var tournoisjeuVideoVM = this._repositoryTournoiJeuVideo.Get(id).Map<TournoiJeuVideoViewModel>();
            return View(tournoisjeuVideoVM);
        }

        // POST: Tournois/Delete/5
        [Authorize(Roles = "Admin, Organisateur")]
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                this._repositoryTournoiJeuVideo.Delete(id);
                this._repositoryTournoiJeuVideo.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
