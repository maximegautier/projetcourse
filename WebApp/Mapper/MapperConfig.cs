﻿using BO.Model;
using BO.Model.Jeux;
using BO.Model.Personnes.Equipes;
using BO.Model.Tournois;
using BO.Personnes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.Models;
using WebApp.Models.Equipes;
using WebApp.Models.Jeux;
using WebApp.Models.Personnes;
using WebApp.Models.Personnes.Equipes;
using WebApp.Models.Tournois;

namespace WebApp.Mapper
{
    public class MapperConfig
    {
        public static void Init()
        {
            AutoMapper.Mapper.Initialize(config =>
            {
                // Création d'un mapper de Joueur vers JoueurViewModel
                config.CreateMap<ApplicationUser, PersonneViewModel>().ForMember(j => j.equipeName, opt => opt.MapFrom(a => a.Equipe.Nom))
                .ForMember(j => j.Mail, opt => opt.MapFrom(a => a.Email))
                .ForMember(j => j.Pseudonyme, opt => opt.MapFrom(a => a.UserName));
                //.ForMember(j => j.Role, opt => opt.MapFrom(a => a.);


                config.CreateMap<ApplicationUser, EditViewPersonne>().ForMember(j => j.equipeName, opt => opt.MapFrom(a => a.Equipe.Nom))
                .ForMember(j => j.Mail, opt => opt.MapFrom(a => a.Email))
                .ForMember(j => j.Pseudonyme, opt => opt.MapFrom(a => a.UserName));

                config.CreateMap<Equipe, EquipeViewModel>();
                config.CreateMap<Equipe, AddEquipeToTournoiViewModel>().ForMember(e=>e.tournoiId,opt=>opt.MapFrom(a=>a.Tournois.Select(t=>t.id.ToString()).ToList()));

                config.CreateMap<Equipe, CreateEditEquipeViewModel>().ForMember(j => j.joueursSelect,opt => opt.MapFrom(a => a.personnes.Select(c => c.Id)));

                config.CreateMap<JeuVideo, JeuVideoViewModel>();
                config.CreateMap<Support, SupportViewModel>();
                config.CreateMap<JeuVideo, CreateEditJeuVideo>().ForMember(j => j.Supports, opt => opt.Ignore())
                                                                .ForMember(j => j.SupportSelected, opt => opt.MapFrom(a => a.Supports.Select(s => s.id.ToString()).ToList()));

                config.CreateMap<Match, MatchViewModel>();
                config.CreateMap<TournoiJeuVideo, TournoiJeuVideoViewModel>();
                config.CreateMap<TournoiJeuVideo, CreateEditTournoisJeuVideoViewModel>().ForMember(t => t.ModeId, opt => opt.MapFrom(a => ((int)a.Mode).ToString()))
                                                                                        .ForMember(t => t.JeuVideos, opt => opt.Ignore())
                                                                                        .ForMember(t => t.JeuVideoId, opt => opt.MapFrom(a => a.JeuVideo.id.ToString()))
                                                                                        .ForMember(t => t.Matchs, opt => opt.Ignore())
                                                                                        .ForMember(t => t.MatchsId, opt => opt.MapFrom(a => a.Matchs.Select(s => s.id.ToString()).ToList()))
                                                                                        .ForMember(t => t.Equipes, opt => opt.Ignore())
                                                                                        .ForMember(t => t.equipesId, opt => opt.MapFrom(a => a.Equipes.Select(s => s.id.ToString()).ToList()));

                // Mapping inverse
                config.CreateMap<PersonneViewModel, ApplicationUser>().ForMember(j => j.Equipe, opt => opt.Ignore())
                .ForMember(j => j.Email, opt => opt.MapFrom(a => a.Mail))
                .ForMember(j => j.UserName, opt => opt.MapFrom(a => a.Pseudonyme));
                config.CreateMap<EditViewPersonne, ApplicationUser>().ForMember(j => j.Equipe, opt => opt.Ignore())
                .ForMember(j => j.Email, opt => opt.MapFrom(a => a.Mail))
                .ForMember(j => j.UserName, opt => opt.MapFrom(a => a.Pseudonyme));

                config.CreateMap<EquipeViewModel, Equipe>();
                config.CreateMap<CreateEditEquipeViewModel, Equipe>();

                config.CreateMap<JeuVideoViewModel, JeuVideo>();
                config.CreateMap<SupportViewModel, Support>();
                config.CreateMap<CreateEditJeuVideo, JeuVideo>().ForMember(j => j.Supports, opt => opt.Ignore());

                config.CreateMap<MatchViewModel, Match>();
                config.CreateMap<TournoiJeuVideoViewModel, TournoiJeuVideo>();
                config.CreateMap<CreateEditTournoisJeuVideoViewModel, TournoiJeuVideo>().ForMember(t => t.JeuVideo, opt => opt.Ignore())
                                                                                        .ForMember(t => t.Matchs, opt => opt.Ignore())
                                                                                        .ForMember(t => t.Equipes, opt => opt.Ignore());

            });
        }
    }
}