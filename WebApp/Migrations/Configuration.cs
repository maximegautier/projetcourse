namespace WebApp.Migrations
{
    using BO;
    using BO.Model;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "WebApp.Models.ApplicationDbContext";
        }

        protected override void Seed(ApplicationDbContext context)
        {
            //  This method will be called after migrating to the latest version.

            if (!context.Roles.Any(r => r.Name == "Admin"))
            {
                var storeRole = new RoleStore<IdentityRole>(context);
                var managerRole = new RoleManager<IdentityRole>(storeRole);

                var role = new IdentityRole { Name = "Admin" };
                managerRole.Create(role);

                role = new IdentityRole { Name = "Joueur" };
                managerRole.Create(role);

                role = new IdentityRole { Name = "Entraineur" };
                managerRole.Create(role);

                role = new IdentityRole { Name = "Organisateur" };
                managerRole.Create(role);
            }

            if (!context.Users.Any(u => u.UserName == "admin"))
            {
                var storeUser = new UserStore<ApplicationUser>(context);
                var managerUser = new UserManager<ApplicationUser>(storeUser);
                var user = new ApplicationUser { UserName = "admin", Email = "admin@admin.fr" };

                managerUser.Create(user, "administrateur");
                managerUser.AddToRole(user.Id, "Admin");
            }

        }
    }
}
