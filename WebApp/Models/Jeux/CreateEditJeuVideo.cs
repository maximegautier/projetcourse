﻿using BO;
using BO.Model.Jeux;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;

namespace WebApp.Models
{
    public class CreateEditJeuVideo
    {
        public int Id { get; set; }
        public string Nom { get; set; }

        [DisplayName("Age Limite")]
        public int AgeLimite { get; set; }
        public List<SelectListItem> Supports { get; set; }
        public List<string> SupportSelected { get; set; }

        public CreateEditJeuVideo()
        {
            IRepository<Support> reposupport = UnityConfig.Container.Resolve<IRepository<Support>>();
            this.Supports = reposupport.GetAll().Select(s => new SelectListItem { Value = s.id.ToString(), Text = s.libelle }).ToList();
        }
    }
}