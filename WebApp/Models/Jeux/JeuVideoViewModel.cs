﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using WebApp.Models.Jeux;

namespace WebApp.Models
{
    public class JeuVideoViewModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public virtual List<SupportViewModel> Supports { get; set; }

        [DisplayName("Age Limite")]
        public int AgeLimite { get; set; }
    }
}