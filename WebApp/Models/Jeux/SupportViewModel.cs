﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApp.Models.Jeux
{
    public class SupportViewModel 
    {
        public int Id { get; set; }

        [DisplayName("Nom")]
        public string libelle { get; set; }
    }
}