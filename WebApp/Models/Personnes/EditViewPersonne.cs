﻿using BO;
using BO.Model;
using BO.Model.Personnes.Equipes;
using BO.Repository;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Unity;

namespace WebApp.Models.Personnes
{
    public class EditViewPersonne : PersonneViewModel
    {
        public string EquipeId { get; set; }

        public List<SelectListItem> EquipesForList { get; set; }

        public List<SelectListItem> RolesForList { get; set; }

        public string RoleId { get; set; }

        public EditViewPersonne()
        {
            IRepository<Equipe> equipeRepository = UnityConfig.Container.Resolve<IRepository<Equipe>>();

            this.EquipesForList = equipeRepository.GetAll().Select(a => new SelectListItem
            {
                Text = a.Nom,
                Value = a.id.ToString(),
            }).ToList();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>());
            this.RolesForList = roleManager.Roles.Select(a => new SelectListItem() { Value = a.Name, Text = a.Name }  ).ToList();
        }
    }
}