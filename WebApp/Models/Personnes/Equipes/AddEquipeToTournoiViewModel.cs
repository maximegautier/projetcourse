﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models.Equipes;
using Unity;
using BO.Repository;
using BO.Model.Personnes.Equipes;
using BO.Model.Tournois;

namespace WebApp.Models.Personnes.Equipes
{
    public class AddEquipeToTournoiViewModel : EquipeViewModel
    {
        public List<string> tournoiId { get; set; }

        public List<SelectListItem> tournoiDispos { get; set; }


        public void InitData()
        {
            IRepository<TournoiJeuVideo> repoTournoi = UnityConfig.Container.Resolve<IRepository<TournoiJeuVideo>>();
            this.tournoiDispos = repoTournoi.GetAll(
                x => this.personnes.Count() ==  (int) x.Mode ).Select(
                s => new SelectListItem
                {
                    Text = s.Libelle,
                    Value = s.id.ToString(),
                }
                ).ToList();
        }
    } 
}