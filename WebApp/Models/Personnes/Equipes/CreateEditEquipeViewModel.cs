﻿using BO.Personnes;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApp.Models.Equipes;
using Unity;
using BO.Model;
using System.ComponentModel;

namespace WebApp.Models.Personnes.Equipes
{
    public class CreateEditEquipeViewModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public string CoachId { get; set; }

        [DisplayName("Joueurs")]
        public List<string> joueursSelect { get; set; }
        public List<SelectListItem> joueursDispo { get; set; }

        public void InitData()
        {
            IRepository<ApplicationUser> repoPersonne = UnityConfig.Container.Resolve<IRepository<ApplicationUser>>();
            this.joueursDispo = repoPersonne.GetAll(a => a.Roles.Select(s => s.RoleId).Contains("c5121754-19af-4e41-b1b9-997b1df59a2d")).Select(
                s => new SelectListItem
                {
                    Text = s.Nom,
                    Value = s.Id,
                    Disabled = s.Equipe != null && s.Equipe.id != this.Id,
                }
                ).ToList();
        }
    }
}