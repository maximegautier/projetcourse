﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using WebApp.Models.Personnes;
using WebApp.Models.Tournois;

namespace WebApp.Models.Equipes
{
    public class EquipeViewModel
    {
        public int Id { get; set; }
        public string Nom { get; set; }
        public int Point { get; set; }
        public string CoachId { get; set; }

        [DisplayName("Joueurs")]
        public List<PersonneViewModel> personnes { get; set; }
    }
}