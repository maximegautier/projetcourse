﻿using BO.Model;
using BO.Model.Personnes;
using BO.Model.Personnes.Equipes;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApp.Models.Equipes;

namespace WebApp.Models.Personnes
{
    public class PersonneViewModel
    {
        public string Id { get; set; }

        [Required]
        [DisplayName("Date de naissance")]
        public DateTime DateNaissance { get; set; }

        [Required]
        public string Nom { get; set; }

        [Required]
        public string Prenom { get; set; }

        [Required]
        [DisplayName("Email")]
        public string Mail { get; set; }

        [Required]
        [DisplayName("Pseudo")]
        public string Pseudonyme { get; set; }

        [DisplayName("Equipe")]
        public string equipeName { get; set; }

        public PersonneViewModel()
        {
            
        }

    }
}