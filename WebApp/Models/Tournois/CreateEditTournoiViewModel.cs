﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApp.Models.Tournois
{
    public class CreateEditTournoiViewModel
    {
        List<int> EquipesId { get; set; }
        List<SelectListItem> EquipesForList { get; set; }
        List<int> TypesTournoiId { get; set; }
        List<SelectListItem> TypesTournoiForList { get; set; }
        List<int> MatchsId { get; set; }
        List<SelectListItem> MatchsForList { get; set; }

    }
}