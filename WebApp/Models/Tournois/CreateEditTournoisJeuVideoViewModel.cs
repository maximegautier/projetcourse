﻿using BO.Model.Jeux;
using BO.Model.Personnes.Equipes;
using BO.Model.Tournois;
using BO.Repository;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Unity;

namespace WebApp.Models.Tournois
{
    public class CreateEditTournoisJeuVideoViewModel
    {
        public int Id { get; set; }

        [DisplayName("Nom")]
        public string Libelle { get; set; }
        public DateTime Date { get; set; }

        [DisplayName("Cash Prize")]
        public float CashPrize { get; set; }
        public string Reglement { get; set; }
        public List<SelectListItem> Equipes { get; set; }
        public List<string> equipesId { get; set; }
        public float Prix { get; set; }
        public List<SelectListItem> Matchs { get; set; }
        public List<string> MatchsId { get; set; }
        public List<SelectListItem> Modes { get; set; }

        [DisplayName("Mode")]
        public string ModeId { get; set; }

        [DisplayName("Nombre d'équipe maximum")]
        public int NombreEquipeMax { get; set; }

        [DisplayName("Jeux Vidéo")]
        public List<SelectListItem> JeuVideos { get; set; }
        public string JeuVideoId { get; set; }

        public void InitList()
        {
            this.Modes = Enum.GetValues(typeof(Mode)).Cast<Mode>().Select(s => new SelectListItem { Value = ((int)s).ToString(), Text = s.ToString() }).ToList();

            IRepository<JeuVideo> repoJeuVideo = UnityConfig.Container.Resolve<IRepository<JeuVideo>>();
            this.JeuVideos = repoJeuVideo.GetAll().Select(s => new SelectListItem { Value = s.id.ToString(), Text = s.Nom }).ToList();
        }
    }
}