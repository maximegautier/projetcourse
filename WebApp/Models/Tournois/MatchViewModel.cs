﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApp.Models.Equipes;

namespace WebApp.Models.Tournois
{
    public class MatchViewModel
    {
        public int Id { get; set; }
        public string NomEquipeGagnante { get; set; }
        public List<EquipeViewModel> Equipes { get; set; }
    }
}