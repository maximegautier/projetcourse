﻿using BO.Model.Tournois;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Web;

namespace WebApp.Models.Tournois
{
    public class TournoiJeuVideoViewModel : TournoiViewModel
    {
        public Mode Mode { get; set; }

        [DisplayName("Nombre d'équipe maximum")]
        public int NombreEquipeMax { get; set; }

        [DisplayName("Jeu Vidéo")]
        public virtual JeuVideoViewModel JeuVideo { get; set; }
    }
}