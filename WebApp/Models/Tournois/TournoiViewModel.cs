﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using WebApp.Models.Equipes;

namespace WebApp.Models.Tournois
{
    public class TournoiViewModel
    {
        public int Id { get; set;  }

        [DisplayName("Equipe gagnante")]
        public string EquipeGagnante { get; set; }

        [DisplayName("Nom")]
        public string Libelle { get; set; }
        public DateTime Date { get; set; }

        [DisplayName("Cash Prize")]
        public float CashPrize { get; set; }
        public string Reglement { get; set; }
        public List<EquipeViewModel> equipes { get; set; }
        public float Prix { get; set; }
        public List<MatchViewModel> Matchs { get; set; }
    }
}